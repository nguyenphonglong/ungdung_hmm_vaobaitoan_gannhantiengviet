file = open("/home/long/Desktop/finally/data/final_data.txt","r")
listAlpha = file.read().splitlines()
"""#print(listAlpha)
print(type(listAlpha))
print(len(listAlpha))
print(listAlpha[0])
print(type(listAlpha[0]))
print(len(listAlpha[0]))
t=listAlpha[0].split()
print(t)
print(len(t))
print(t[0].split("/")[0])
print(t[1].split("/")[0])
print(type(t[2].split("/")[0]))"""
dulieudatrain=[]
for i in range(len(listAlpha)):
    t = listAlpha[i].split()
    for j in range(len(t)):
        #print(t[j].split("//")[0])
        dulieudatrain.append(t[j].split("//")[0])
#print(dulieudatrain)
#-----------------------------------------
import pickle
import sys
import math
import sys
import os
import time
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMainWindow
class ReadFiles:

    def __init__(self, file_name):
        self.file_name = file_name
        # list of list of tuples of sentences.
        self.all_tuples = []

        # dictionary where key is the word and value is a set of all its tags
        self.word_tags = {}

        # set to hold the unique tags in the training corpus
        self.unique_tags = set()

    def word_tag_tuples(self):
        # Reading the data corpus
        with open(self.file_name, "r",encoding='utf-8') as f:
            for line in f:
                # list of tuples for a sentence
                list_tuples_sentence = []
                tuple_words = line.split()

                # for word
                for tup in tuple_words:
                    split_word = tup.split('//')
                    split_word_word = "//".join(split_word[:-1])
                    split_word_tag = split_word[-1]

                    # saving the unique tags in the training data in the corpus
                    if split_word_tag not in self.unique_tags:
                        self.unique_tags.add(split_word_tag)

                    # tuple for word-tag pair
                    tuples_for_sentence = (split_word_word, split_word_tag)
                    # add the tuple to the list
                    list_tuples_sentence.append(tuples_for_sentence)

                    # adding the all the corresponding tags of a word to a dictionary
                    if split_word_word in self.word_tags:
                        tags_set = self.word_tags[split_word_word]
                        tags_set.add(split_word_tag)
                    else:
                        tags_set = set([split_word_tag])
                        self.word_tags[split_word_word] = tags_set

                # adding (*,*) tuple at the start of a sentence
                # for trigram transition probability computation
                # word-tag
                list_tuples_sentence.insert(0, ('*', '*'))
                list_tuples_sentence.insert(0, ('*', '*'))
                self.all_tuples.append(list_tuples_sentence)
        self.word_tags["*"] = "*"
        return self.all_tuples

    def word_raw(self):

        words_sents = []

        # Reading the raw text file
        with open(self.file_name, "r",encoding='utf-8') as f:
            for line in f:
                # list of tuples for a sentence
                words_sent = []

                words = line.split()
                for word in words:
                    words_sent.append(word)

                # adding (*,*) tuple at the start of a sentence
                # for trigram transition probability computation
                # word-tag
                words_sent.insert(0, ('*'))
                words_sent.insert(0, ('*'))

                words_sents.append(words_sent)

        return words_sents
#!/usr/bin/env python
# -*- coding: utf-8 -*-

global bienphu

class ViterbiDecode:

    def __init__(self):

        # dictionary to store the emission probability for a given word-tag combination
        self.emission_probabilities = {}

        # dictionary to store the transition probability for a given trigram combination
        self.transition_probabilities = {}

        # dictionary where key is the word and value is a set of all its tags
        self.word_tags_set = {}

        # dictionary to store the probabilities in the sequence
        # maximum probability to a given word-tag sequence
        self.word_tag_viterbi_probability = {}

        # saving the index-tag(word-tag) for the last word of the sentence
        # with maximum probability
        self.index_tag_key = None

        # a set of all the unique tags in the training corpus
        self.unique_tags = set()
        self.bigram_counts = None

    def recursive_probability_cal_sequence(self, word_sequence, index, word_tag_i):
        # base case: if the index is the start tag.
        # return the previous probability as 0.
        if index == 1:
            return 0.0

        # if the probability already exists then return that from the dictionary
        if (index, word_tag_i) in self.word_tag_viterbi_probability:
            return self.word_tag_viterbi_probability[index, word_tag_i][0]

        # word-tags from index-1
        if word_sequence[index - 1] in self.word_tags_set:
            word_tags_i_1 = self.word_tags_set[word_sequence[index - 1]]
        else:
            word_tags_i_1 = self.unique_tags

        # word-tags from index-2
        if word_sequence[index - 2] in self.word_tags_set:
            word_tags_i_2 = self.word_tags_set[word_sequence[index - 2]]
        else:
            word_tags_i_2 = self.unique_tags

        max_viterbi_prob = -1000000.0

        back_pointer_tag = "*"

        # iterating through all word-tag combination from previous indexes recursively
        for word_tag_i_1 in word_tags_i_1:
            for word_tag_i_2 in word_tags_i_2:
                viterbi_prob = 0.0

                if (word_tag_i_2, word_tag_i_1, word_tag_i) in self.transition_probabilities:
                    transition_prob = self.transition_probabilities[(
                        word_tag_i_2, word_tag_i_1, word_tag_i)]
                else:
                    if (word_tag_i_2, word_tag_i_1) in self.bigram_counts:
                        transition_prob = math.log(
                            1.0 / float(self.bigram_counts[(word_tag_i_2, word_tag_i_1)] + len(self.unique_tags)))
                    else:
                        transition_prob = math.log(
                            1.0 / float(len(self.unique_tags)))

                if (word_sequence[index], word_tag_i) not in self.emission_probabilities:
                    transition_prob = 0.0
                    viterbi_prob = self.recursive_probability_cal_sequence(word_sequence, index - 1, word_tag_i_1) + \
                        transition_prob
                else:
                    # Viterbi probability for any sequence is
                    # Viterbi Probability from previous words +
                    # Emission probability for given word-tag sequence +
                    # Transition probability for tag(i),tag(i-1),tag(i-2)
                    viterbi_prob = self.recursive_probability_cal_sequence(word_sequence, index - 1, word_tag_i_1) + \
                        self.emission_probabilities[(
                            word_sequence[index], word_tag_i)] + transition_prob

                # Save the maximum probability till now of all the combinations and the back pointer
                # parent pointer(i-1)
                if max_viterbi_prob < viterbi_prob:
                    max_viterbi_prob = viterbi_prob
                    back_pointer_tag = word_tag_i_1

        self.word_tag_viterbi_probability[index, word_tag_i] = (
            max_viterbi_prob, (index - 1, back_pointer_tag))
        return max_viterbi_prob

    def load(self):
        with open("models/hmmmodel.pkl", "rb") as f:
            data = pickle.load(f)
            self.transition_probabilities = data["transition"]
            self.emission_probabilities = data["emission"]
            self.word_tags_set = data["word2tags"]
            self.unique_tags = data["unique_tags"]
            self.bigram_counts = data["bigram"]

    def viterbi_algorithm(self, file_name):
        output = open("models/hmmoutput.txt", "w",encoding='utf-8')
        read_files = ReadFiles(file_name)
        words_sents = read_files.word_raw()
        for words_sent in words_sents:
            max_viterbi_prob = -1000000.0
            self.word_tag_viterbi_probability = {}
            # length of the current sentence
            len_words_sent = len(words_sent)
            if words_sent[len_words_sent - 1] in self.word_tags_set:
                word_tags_i = self.word_tags_set[words_sent[len_words_sent - 1]]
            else:
                word_tags_i = self.unique_tags

            for word_tag_i in word_tags_i:
                viterbi_prob = self.recursive_probability_cal_sequence(
                    words_sent, len_words_sent - 1, word_tag_i
                )
                # Saving the last pointer of the word in the sentence
                if max_viterbi_prob < viterbi_prob:
                    max_viterbi_prob = viterbi_prob
                    self.index_tag_key = (len_words_sent - 1, word_tag_i)

            tagged_sentence = []
            while self.index_tag_key[0] >= 2:
                tagged_sentence.insert(
                    0, words_sent[self.index_tag_key[0]] + "//" + self.index_tag_key[1])
                self.index_tag_key = self.word_tag_viterbi_probability[self.index_tag_key][1]

            #output.write(" ".join(v.encode("utf-8") for v in tagged_sentence) + "\n")
            for i in range(len(tagged_sentence)):
                output.writelines(str(tagged_sentence[i].encode("utf-8")))
                #output.writelines(str(v.encode("utf-8") for v in tagged_sentence))
                output.write(" ")
            #d=open("../data/Brown_tagged_train.txt"."r")
            sbc=[]
            """with open("C:/Users/Dell/Desktop/TV/src/Brown_tagged_train.txt", "r") as myfile:
                data1 = myfile.read()
                sbc.append(data1)
                #print(myfile)
            #print(sbc)
            #print(len(sbc))
            sbc=str(sbc)
            mmm=sbc.split(" ")
            print(mmm)
            for i in range(len(mmm)):
                n = tagged_sentence[i].split("/a")
                print(n)"""
            data_Trained=["I","am"]
            #print(data_Trained)
            a = tagged_sentence[i].split("//")
            bienphu=[]
            #tagged_sentence=[]
            for i in range(len(tagged_sentence)):
                a = tagged_sentence[i].split("//")
                if a[0] in data_Trained:
                    pass
                else:
                    a[1]="OTher"
                bienphu.append(a)
            tagged_sentence=bienphu
            print(tagged_sentence)
        output.close()
    def viterbi_algorithm1(self, file_name):
        global tagged_sentence, bienphu
        output = open("models/hmmoutput.txt", "w",encoding='utf-8')
        read_files = ReadFiles(file_name)
        words_sents = read_files.word_raw()
        for words_sent in words_sents:
            max_viterbi_prob = -1000000.0
            self.word_tag_viterbi_probability = {}
            # length of the current sentence
            len_words_sent = len(words_sent)
            if words_sent[len_words_sent - 1] in self.word_tags_set:
                word_tags_i = self.word_tags_set[words_sent[len_words_sent - 1]]
            else:
                word_tags_i = self.unique_tags

            for word_tag_i in word_tags_i:
                viterbi_prob = self.recursive_probability_cal_sequence(
                    words_sent, len_words_sent - 1, word_tag_i
                )
                # Saving the last pointer of the word in the sentence
                if max_viterbi_prob < viterbi_prob:
                    max_viterbi_prob = viterbi_prob
                    self.index_tag_key = (len_words_sent - 1, word_tag_i)

            tagged_sentence = []
            while self.index_tag_key[0] >= 2:
                tagged_sentence.insert(
                    0, words_sent[self.index_tag_key[0]] + "//" + self.index_tag_key[1])
                self.index_tag_key = self.word_tag_viterbi_probability[self.index_tag_key][1]

            #output.write(" ".join(v.encode("utf-8") for v in tagged_sentence) + "\n")
            for i in range(len(tagged_sentence)):
                output.writelines(str(tagged_sentence[i].encode("utf-8")))
                #output.writelines(str(v.encode("utf-8") for v in tagged_sentence))
                output.write(" ")
            #d=open("../data/Brown_tagged_train.txt"."r")
            sbc=[]
            """with open("C:/Users/Dell/Desktop/TV/src/Brown_tagged_train.txt", "r") as myfile:
                data1 = myfile.read()
                sbc.append(data1)
                #print(myfile)
            #print(sbc)
            #print(len(sbc))
            sbc=str(sbc)
            mmm=sbc.split(" ")
            print(mmm)
            for i in range(len(mmm)):
                n = tagged_sentence[i].split("/a")
                print(n)"""
            data_Trained=["I","am"]
            #print(data_Trained)
            a = tagged_sentence[i].split("//")
            bienphu=[]
            #tagged_sentence=[]
            for i in range(len(tagged_sentence)):
                a = tagged_sentence[i].split("//")
                if a[0] in dulieudatrain:
                    pass
                else:
                    a[1]="OTher"
                bienphu.append(a)
            tagged_sentence=bienphu
            #print(tagged_sentence)
        output.close()
        return bienphu

if __name__ == "__main__":
    file_name = "data/long.txt.txt"
    #file_name="long.txt.txt"
    viterbi = ViterbiDecode()
    viterbi.load()
    viterbi.viterbi_algorithm(file_name)
