from predict import *
import sys
import os
import time
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMainWindow
class Team_Champion(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'Mô hình Markov ẩn trong gán nhãn từ loại'
        self.model_path = 'pretrained/final_model.ckpt'
        self.left = 240
        self.top = 200
        self.width = 800
        self.height = 490
        self.initUI()


    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setMaximumSize(self.width, self.height)
        self.setMinimumSize(self.width, self.height)
        # 4 BUTTON ben trai
        # BUTTON OPEN FOLDER
        """self.btn1 = QPushButton("OPEN FOLDER", self)
        self.btn1.clicked.connect(self.Open_Folder)
        self.btn1.setGeometry(20, 20, 120, 60)"""
        self.anh1 = QPushButton(self)

        self.anh1.setGeometry(10,20, 151, 459)
        self.anh1.setIcon(QIcon('Giaithich.png'))
        self.anh1.setIconSize(QSize(151, 459))
        self.text_edit_widget = QPlainTextEdit(self)

        # Change font, colour of text entry box
        self.text_edit_widget.setStyleSheet(
            """QPlainTextEdit {background-color: #FFFFFF;
                               color:  	#000000;
                               font-size: 19pt; 
                               font-family: Courier;}""")
        self.text_edit_widget.setGeometry(180, 20, 600, 200)
        self.btn1 = QPushButton("START", self)
        self.btn1.clicked.connect(self.Open_Folder)
        self.btn1.setGeometry(420, 230, 120, 30)
        self.result = QPlainTextEdit(self)

        # Change font, colour of text entry box
        self.result.setStyleSheet(
            """QPlainTextEdit {background-color: #FFFFFF;
                               color:  	#000000;
                               font-size: 19pt; 
                               font-family: Courier;}""")
        self.result.setGeometry(180, 280, 600, 199)
        self.show()



    @pyqtSlot()
    def Open_Folder(self):

        """self.folder = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        print(self.folder)"""
        textDemmo=self.text_edit_widget.document().toPlainText()
        f = open("data/long.txt.txt", "w+",encoding="utf-8")
        f.write(textDemmo)
        f.close()

        file_name = "data/long.txt.txt"

        viterbi2 = ViterbiDecode()
        viterbi2.load()
        b = viterbi2.viterbi_algorithm1(file_name)
        resultOfTeam=""
        for i in range(len(b)):
            resultOfTeam+=b[i][0]+"("+b[i][1]+")"+" "
        self.result.setPlainText(resultOfTeam)
        #self.result.setPlainText(b[0][1])
if __name__ == '__main__':







    app = QApplication(sys.argv)
    ex = Team_Champion()
    sys.exit(app.exec_())
